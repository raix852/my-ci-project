import sqlite3
import os
import re
import logging
import time

class SqliteDB(object):
    sql_files = {
        "aritcles": "create_articles_table.sql"
    }

    db_name = 'aritcles.db'

    def __init__(self):

        db_path = self.db_name
        self.sql_conn = sqlite3.connect(db_path)
        self._create_tables(self.sql_conn.cursor())

    def _create_tables(self, cursor):
        for name, filename in self.sql_files.items():
            self._create_table(name, filename, cursor)

    def _create_table(self, tablename, filename, cursor):
        logging.info("Create table %s.", tablename)
        with open(self._get_sql_file(filename), 'r') as f:
            sqlContext = f.read()

        cursor.execute(sqlContext)
        self.sql_conn.commit()

    def _get_sql_file(self, name):
        sql_dir = os.environ.get('SQL_PATH')
        print sql_dir
        if sql_dir:
            return os.path.join(sql_dir, name)
        else:
            dirname = os.path.dirname(os.path.realpath(__file__))
            return os.path.join(dirname, 'sql', name)

    def exist_article_id(self, article_id):
        c = self.sql_conn.cursor()
        c.execute("SELECT title FROM aritcles WHERE article_id = ?", (article_id, ))

        data = c.fetchone()
        if data is None:
            return False
        else:
            return True

    def store_article_data(self, data):
        c = self.sql_conn.cursor()
        link = data[1]

        c.execute("INSERT INTO aritcles VALUES (?, ?, ?, ?, ?, ?)", data)
        self.sql_conn.commit()

    def get_article_count(self):
        c = self.sql_conn.cursor()
        c.execute("SELECT count(*) FROM aritcles")
        return c.fetchone()

    def get_all_aritcle_data(self):
        c = self.sql_conn.cursor()
        c.execute("SELECT * FROM aritcles")
        return c.fetchall()

    def get_article_data(self, article_id):
        c = self.sql_conn.cursor()

        sql = "SELECT * FROM aritcles WHERE article_id = ?"
        c.execute(sql, (article_id, ))
        return c.fetchone()

    def get_articles(self):
        c = self.sql_conn.cursor()

        sql = "SELECT content FROM aritcles"
        c.execute(sql)
        return c.fetchall()

    def get_articles_by(self, column, condition):
        c = self.sql_conn.cursor()
        if re.match('[\w_]+', column):
            sql = "SELECT content FROM aritcles WHERE %s = ?" % (column)
            c.execute(sql, (condition, ))
            return c.fetchall()
        else:
            logging.error("Column name contains invalid characters.")
            return None

    def delete_article_data(self, article_id):
        c = self.sql_conn.cursor()
        sql = "DELETE FROM aritcles WHERE article_id = ?"
        c.execute(sql, (article_id, ))
        self.sql_conn.commit()

    def close(self):
        self.sql_conn.close()
